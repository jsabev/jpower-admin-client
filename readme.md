CustomDNS Client
================

## About

A proof-of-concept utility to monitor A records for zones hosted in 
[CustomDNS](https://www.customdns.ca). It automatically sets the A record 
to the host IP address of the system running the CustomDNS client.

## Configuration

Enter the zones that need to be monitored in the application.yml file. Set 
the api key to the GUID from your CustomDNS account (under Account Settings) 

## Building

Run ``mvn package`` to build the application. 

## Running 
Running the resulting .jar file will scan your zones every 5 minutes and update 
any ``A`` records whose IP address is different than the IP address of the host 
running the CustomDNS client. 

The CustomDNS client has been tested on OpenJDK 8,10 and 11.
