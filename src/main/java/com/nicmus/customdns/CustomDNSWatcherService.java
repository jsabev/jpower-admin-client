/** *****************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *****************************************************************************
 */
package com.nicmus.customdns;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.nicmus.customdns.config.WatchedZonesConfig;
import com.nicmus.customdns.config.model.Record;
import com.nicmus.customdns.config.model.Zone;

@Service
public class CustomDNSWatcherService {

    private Logger logger = LoggerFactory.getLogger(CustomDNSWatcherService.class);

    private static final int SCAN_FREQ = 1000 * 60 * 5; //5 minutes minimum scan frequency

    @Autowired
    private CustomDNSClient customDNSClient;

    @Autowired
    private PublicIPService publicIPService;

    @Autowired
    private WatchedZonesConfig watchedZonesConfig;

    /**
     * Get a mapping of zones->zone record that are watched for changes
     *
     * @return
     */
    private Map<String, List<ZoneRecord>> getZonesToWatch() {
        Map<String, List<ZoneRecord>> watchedZones = new LinkedHashMap<>();
        List<Zone> zonesToWatch = watchedZonesConfig.getZones();

        for (Zone zone : zonesToWatch) {
            List<Record> zoneRecordsToWatch = zone.getRecords();
            final String zoneName = zone.getName();
            final int zoneRecordCount = this.customDNSClient.getZoneRecordCount(zoneName);
            final int increment = 50;
            int startIndex = 0;
            do {
                List<ZoneRecord> zoneRecords = this.customDNSClient.getZoneRecords(zoneName, startIndex, increment);
                for (Record recordToWatch : zoneRecordsToWatch) {
                    //find the record in the zone listing
                    String recordName = recordToWatch.getName();
                    if (!recordName.toLowerCase().endsWith(zone.getName().toLowerCase())) {
                        recordName += "." + zone.getName().toLowerCase();
                    }
                    final String toWatch = recordName;

                    //find the record to watch in the ZoneRecords list as returned 
                    //by the REST API Call
                    Optional<ZoneRecord> findFirst = zoneRecords.stream().filter(
                            zr -> zr.getName().equalsIgnoreCase(toWatch)
                            && zr.getType().equalsIgnoreCase(recordToWatch.getType())).findFirst();

                    if (findFirst.isPresent()) {
                        ZoneRecord zoneRecord = findFirst.get();
                        if (watchedZones.containsKey(zone.getName())) {
                            watchedZones.get(zone.getName()).add(zoneRecord);
                        } else {
                            List<ZoneRecord> watchedRecords = new ArrayList<>();
                            watchedRecords.add(zoneRecord);
                            watchedZones.put(zone.getName(), watchedRecords);

                        }
                    }
                }
                startIndex += increment;                
            } while (startIndex < zoneRecordCount);
        }
        return watchedZones;
    }

    /**
     *
     */
    @Scheduled(fixedDelay = SCAN_FREQ)
    public void watchForChanges() {
        this.logger.debug("Starting scan of zones looking for changes");
        String publicIP = this.publicIPService.getPublicIP();
        //map of zone->  ZoneRecords 
        Map<String, List<ZoneRecord>> watchedZones = this.getZonesToWatch();
        watchedZones.forEach((zone, zoneRecords) -> {
            zoneRecords.forEach(zoneRecord -> {
                if (!zoneRecord.getContent().equalsIgnoreCase(publicIP)) {
                    //add the new zone to CUSTOMDNS
                    zoneRecord.setContent(publicIP);
                    try {
                        int newId = this.customDNSClient.createZone(zone, zoneRecord);
                        this.customDNSClient.deleteZoneRecord(zone, zoneRecord.getId());
                        zoneRecord.setId(newId);
                        this.logger.info("Successfully updated {}. New IP: {}", zoneRecord.getName(), zoneRecord.getContent());
                    } catch (IOException e) {
                        this.logger.error("Error in updating Zone {}, Record {}", zone, zoneRecord.getName());
                        this.logger.error("", e);                        
                    }
                }
            });
        });
        this.logger.debug("Finished scanning zones looking for changes");
    }

}
