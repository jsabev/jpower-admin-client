/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.customdns;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.nicmus.customdns.config.APIConfig;

@SpringBootApplication
@EnableScheduling
public class CustomdDNSRecordWatcher implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(CustomdDNSRecordWatcher.class);
		
	@Autowired
	private APIConfig apiConfig;
	
	@Bean
	public HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(this.apiConfig.getHeaderName(), this.apiConfig.getKey());
		return headers;
	}
	
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Override
	public void run(String... args) throws Exception {
		this.logger.info("CustomDNS Watch Agent running");
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(CustomdDNSRecordWatcher.class);
		app.run(args);
	}
	
	
}
