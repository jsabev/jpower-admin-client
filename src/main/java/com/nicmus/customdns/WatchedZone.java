/*******************************************************************************
 * Copyright (C) 2019 nicmus inc. (jivko.sabev@gmail.com)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package com.nicmus.customdns;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author U6033789
 *
 */
public class WatchedZone {
	private String zoneName;
	private Map<Integer, ZoneRecord> records;
	
	public WatchedZone(String zoneName) {
		this.zoneName = zoneName;
		this.records = new LinkedHashMap<>();
	}

	/**
	 * @return the zoneName
	 */
	public String getZoneName() {
		return this.zoneName;
	}

	/**
	 * @return the records
	 */
	public Map<Integer, ZoneRecord> getRecords() {
		return this.records;
	}
	
	
	
	
}
